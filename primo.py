            # autora = "Karen Paladines"
            # email  = "karen.paladines@unl.edu.ec"

# Crear una función para determinar si un número es primo o no.

import math

def es_numero_primo(num):

    if (num <=1):
        return False

    for i in range(2, math.ceil(math.sqrt(num)) + 1):
         if (num % i == 0 and i != num):
            return False
    return True

while True:
    try:
        num = int(input("inserta un numero"))
        if num == 0:
            break
        if es_numero_primo(num):
            print("El numero es primo", num)
        else:
            print("El numero  NO es primo", num)
    except:
        print("El numero tiene que ser entero")
